# Maintainer: Fabio 'Lolix' Loli <fabio.loli@disroot.org> -> https://github.com/FabioLolix
# Contributor: Stefano Capitani <stefanoatmanjarodotorg>
# Contributor: Florian Pritz <f-p@gmx.at>
# Contributor: Pellegrino Prevete <pellegrinoprevete@gmail.com>

pkgname=inxi
_pkgver=3.3.27-1
pkgver=${_pkgver//-/.}
pkgrel=1
pkgdesc="Full featured CLI system information tool"
arch=(any)
_ns="smxi"
url="https://${_ns}.org/docs/${pkgname}.htm"
license=(GPL3)
depends=(coreutils pciutils perl procps-ng)
makedepends=()
optdepends=(
        "bluez-tools: bt-adapter: -E bluetooth data (if no hciconfig)"
        "bluez-utils-compat: hciconfig: -E bluetooth HCI data"
        "bind: -i wlan IP"
        "dmidecode: ${pkgname} -M if no sys machine data"
        "file: ${pkgname} -o unmounted file system"
        "freeipmi: ipmi-sensors: -s IPMI sensors"
        "hddtemp: ${pkgname} -Dx show hdd temp"
        "iproute2: ${pkgname} -i ip lan"
        "ipmitool: -s IPMI sensors"
        "kmod: ${pkgname} -Ax,-Nx module version"
        "lm_sensors: ${pkgname} -s sensors output"
        "mesa-utils: ${pkgname} -G glx info"
        "net-tools: ${pkgname} -i ip lan-deprecated"
        "perl-io-socket-ssl: -U; -w,-W; -i (if dig not installed)"
        "perl-cpanel-json-xs: --output json - required for export"
        "perl-json-xs: --output json - required for export (legacy)"
        "perl-xml-dumper: --output xml - Crude and raw"
        "systemd-sysvcompat: ${pkgname} -I runlevel"
        "sudo: ${pkgname} -Dx hddtemp-user;-o file-user"
        "tree: --debugger 20,21 /sys tree"
        "upower: -sx attached device battery info"
        "usbutils: ${pkgname} -A usb audio;-N usb networking"
        "wmctrl: -S active window manager (not all wm)"
        "xorg-xdpyinfo: ${pkgnam} -G multi screen resolution"
        "xorg-xprop: ${pkgname} -S desktop data"
        "xorg-xrandr: ${pkgname} -G single screen resolution"
)
_repo_url="https://github.com/${_ns}/${pkgname}"
source=(
  # "git+${_repo_url}#tag=${_pkgver}"
  "${_repo_url}/archive/refs/tags/${_pkgver}.tar.gz"
)
sha256sums=('35207195579261ddfe59508fdc92d40902c91230084d2b98b4541a6f4c682f63')

package() {
  cd "${pkgname}-${_pkgver}"
  install -D -m755 $pkgname "${pkgdir}/usr/bin/$pkgname"
  install -D -m644 $pkgname.1 "${pkgdir}/usr/share/man/man1/$pkgname.1"
}

